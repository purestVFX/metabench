MetaBench

### What is this repository for? ###

Simple python script to test meta data performance of disks.  

Basicaly it creates allot of small files, changes them, and deletes them - timeing each operation.

It is not super scientific - many things can skew the result (eg server load), but gives a vague idea of how fast a disk is.

### How do I get set up? ###

clone this repo:

    git clone https://bitbucket.org/purestVFX/metabench.git

run the python script

    python run_bench.py [path] [path]...


example:

    rupertt@ws-078 MINGW64 ~/dev/metabench (master)
    $ python run_bench.py c:/
    starting test     8000 files
    starting test for  c:/
    creation: 11.09 sec
    reading : 1.24 sec
    modify  : 2.16 sec
    deletion: 6.96 sec



### Who do I talk to? ###

Rupert Thorpe