import os
import time
import uuid
import shutil
import sys


scriptStarted = time.time()


test_paths = sys.argv[1:]
testSize = 20

print("starting test     "+str(testSize**3)+" files\n")
sys.stdout.flush()

for aPath in test_paths:
    if not os.path.exists(aPath):
        print aPath, "  does not exist\n"

    test_started = time.time()
    print("\nstarting test for  "+aPath)
    sys.stdout.flush()

    create_started = time.time()
    testRoot = aPath + "/metabench_tmp/"
    os.mkdir(testRoot)
    for i in range(testSize):
        dirA = str(uuid.uuid4())
        os.mkdir(testRoot+dirA)
        for i in range(testSize):
            dirB = str(uuid.uuid4())
            os.mkdir(testRoot+dirA+"/"+dirB)
            for i in range(testSize):
                filename = str(uuid.uuid4())+".txt"
                fh = open(testRoot+dirA+"/"+dirB+"/"+filename, "w")
                fh.write(str(uuid.uuid4()))
                fh.close()

    create_finished = time.time()

    read_started = time.time()
    for aDir in os.listdir(testRoot):
        dirA = testRoot+"/"+aDir
        for bDir in os.listdir(dirA):
            dirB = dirA+"/"+bDir
            for aFile in os.listdir(dirB):
                filepath = dirB+"/"+aFile
                fh = open(filepath)
                tempval =fh.read()
                fh.close()
    read_finished = time.time()

    modify_started = time.time()
    for aDir in os.listdir(testRoot):
        dirA = testRoot+"/"+aDir
        for bDir in os.listdir(dirA):
            dirB = dirA+"/"+bDir
            for aFile in os.listdir(dirB):
                filepath = dirB+"/"+aFile
                fh = open(filepath, "a")
                fh.write("\nmod\n")
                fh.close()
    modify_finished = time.time()

    #####cleanup:
    delete_started = time.time()
    shutil.rmtree(testRoot)
    delete_finished = time.time()

    print ("creation: %.2f sec" % (create_finished-create_started))
    print ("reading : %.2f sec" % (read_finished-read_started))
    print ("modify  : %.2f sec" % (modify_finished-modify_started))
    print ("deletion: %.2f sec" % (delete_finished-delete_started))
    sys.stdout.flush()